import subprocess
import optparse
import re



def get_arg():
    parser = optparse.OptionParser()
    parser.add_option("-i", "--interface", dest="interface", help="name of the interface that you want to change")
    parser.add_option("-m", "--mac", dest="new_mac", help="New mac address")
    (options, arguments)= parser.parse_args()
    if not options.interface:
        parser.error('[-] please specify an interface, use --help to learn more')
    elif not options.new_mac:
        parser.error("[-] please specify a mac address, use --help to learn more")
    return options


def mac_changer(interface, new_mac):
    print("[+]changing mac of " + interface + " to " + new_mac)
    subprocess.call(['ifconfig', interface, 'down'])
    subprocess.call(['ifconfig', interface, 'hw', 'ether', new_mac])
    subprocess.call(['ifconfig', interface, 'up'])


def get_current_mac(interface):
    ifconfig_result = subprocess.check_output(["ifconfig", options.interface])
    mac_address_search_result = re.search(r"\w\w:\w\w:\w\w:\w\w:\w\w:\w\w", ifconfig_result)
    if mac_address_search_result:
        return(mac_address_search_result.group(0))
    else:
        print("[-] could not read mac address")

options = get_arg()
current_mac=get_current_mac(options.interface)
print("current mac =" + str(current_mac))
mac_changer(options.interface, options.new_mac)


current_mac= get_current_mac(options.interface)
if current_mac== options.new_mac:
    print("[+] mac changed successfully")
else:
    print("[-] mac did not chnage")
    
    